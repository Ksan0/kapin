class Property(object):
    def __init__(self, value, client=False):
        self.value = value if not isinstance(value, Property) else value.value
        self.client = client

    def client_value(self):
        if isinstance(self.value, str):
            return '"{}"'.format(self.value)

        return self.value


class ConfigsHolder(object):
    client = {}


def conf(cls):
    client_properties = {}

    for key in [x for x in cls.__dict__]:
        p = getattr(cls, key)
        if isinstance(p, Property):
            if p.client:
                client_properties[key] = p.client_value()

            setattr(cls, key, p.value)

    setattr(ConfigsHolder, cls.__name__, cls)
    if client_properties:
        ConfigsHolder.client[cls.__name__] = client_properties

    return cls
