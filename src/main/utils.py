from src.factory import Factory
from src.conf.core import ConfigsHolder
from src.ajax.forms import AjaxFormsHolder


def build_context():
    return {
        'Factory': Factory,
        'Configs': ConfigsHolder,
        'AjaxForms': AjaxFormsHolder
    }
