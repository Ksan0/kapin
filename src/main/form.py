from django import forms


class Form(forms.Form):
    def __init__(self, action_url, **kwargs):
        if 'auto_id' not in kwargs:
            kwargs['auto_id'] = False

        super().__init__(**kwargs)

        self.form_id = (self.__module__ + "_" + self.__class__.__name__).replace(".", "_")
        self.form_name = self.__class__.__name__

        self.class_name = ""
        self.action_url = action_url

    def set_class_name(self, cls):
        self.class_name = cls
