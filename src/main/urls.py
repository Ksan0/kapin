from django.conf.urls import url, include

from src.factory import Factory
from . import views

urlpatterns = [
    url(Factory.URLBuilder.build_index_page_pattern(), views.index),
    url(Factory.URLBuilder.build_hello_page_pattern(), views.hello),
    url(Factory.URLBuilder.build_user_page_pattern(), views.user),
    url(Factory.URLBuilder.build_settings_page_pattern(), views.settings),

    url(Factory.URLBuilder.build_ajax_folder_pattern(), include('src.ajax.urls'))
]
