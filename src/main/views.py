from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render

from src.db.model_user import User
from src.factory import Factory

from .utils import build_context


def index(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(Factory.URLBuilder.build_user_page_url(request.user.id))
    else:
        return HttpResponseRedirect(Factory.URLBuilder.build_hello_page_url())


def hello(request):
    return render(request, 'pages/hello.html', build_context())


def user(request, user_id):
    try:
        owner = User.objects.get(id=user_id)
    except User.DoesNotExist:
        raise Http404

    context = build_context()
    context.update({
        'owner': owner
    })
    return render(request, 'pages/user.html', context)


def settings(request):
    return render(request, 'pages/settings.html', build_context())
