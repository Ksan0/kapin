from django.db import models

from src.conf.config import RedisConfig, CalendarConfig
from src.factory import Factory

from . import bigint


# noinspection PyMethodMayBeStatic
class User(models.Model):
    id = bigint.BigAutoField(primary_key=True)

    is_superuser = models.BooleanField(default=False)

    signup_date = models.DateField(auto_now_add=True)

    name = models.CharField(max_length=50)
    calendar_type = models.SmallIntegerField(default=CalendarConfig.CALENDAR_TYPE_DEFAULT_VALUE)

    vk_id = bigint.BigIntegerField(unique=True)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(User, self).save(force_insert, force_update, using, update_fields)
        Factory.DBCache.update_object(self, RedisConfig.DB_CACHE_USER_EXPIRE)

    def is_authenticated(self):
        return True


# noinspection PyMethodMayBeStatic
class AnonymousUser(object):
    def is_authenticated(self):
        return False
