from django.db import models
from src.db.model_user import User

from . import bigint


class CalendarData(models.Model):
    # грязный хак. на самом деле PRIMARY KEY (user, date)
    # но пусть django думает, что PRIMARY KEY (user)
    # чтобы не пыталась взаимодействовать с несуществующим id
    user = bigint.BigForeignKey(User, primary_key=True)
    date = models.DateField()
    value = models.IntegerField()
    timewarn = models.BooleanField(default=False)
