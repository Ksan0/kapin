from src.factory import Factory
from src.db.model_user import AnonymousUser


class Session(object):
    def __init__(self):
        self.__action = 0

    def set_signin(self):
        self.__action = 1

    def is_signin(self):
        return self.__action == 1

    def set_signout(self):
        self.__action = 2

    def is_signout(self):
        return self.__action == 2


def signin(request, user):
    request.user = user
    request.COOKIES['uid'] = user.id
    request.COOKIES['sid'] = Factory.SessionStorage.generate_session(user.id)
    request.session.set_signin()


def signout(request):
    request.user = AnonymousUser()
    request.COOKIES.pop('uid', None)
    request.COOKIES.pop('sid', None)
    request.session.set_signout()
