from django.conf.urls import url

from src.factory import Factory

from . import views


urlpatterns = [
    url(r'^signin_vk$', views.signin, name=Factory.URLBuilder.build_ajax_signin_vk_name()),
    url(r'^signout$', views.signout, name=Factory.URLBuilder.build_ajax_signout_name()),
    url(r'^download_calendar$', views.download_calendar, name=Factory.URLBuilder.build_ajax_download_calendar_name()),
    url(r'^upload_calendar$', views.upload_calendar, name=Factory.URLBuilder.build_ajax_upload_calendar_name()),
    url(r'^update_settings$', views.update_settings, name=Factory.URLBuilder.build_ajax_update_settings_name())
]
