from Crypto.Hash import MD5
from django.db import IntegrityError, Error as DjDBError

from src.conf import djconfig
from src.auth import auth
from src.conf.config import VkConfig
from src.db.model_user import User
from src.db.model_calendar_data import CalendarData
from src.factory import Factory

from .decor import decor_authenticated, decor_valid_form
from .forms import SigninVkForm, DownloadCalendarForm, UploadCalendarForm, SignoutForm, UpdateSettingsForm
from .utils import redirect, json_response


@decor_valid_form(SigninVkForm)
def signin(request, form):
    sig_raw = 'expire={expire}mid={mid}secret={secret}sid={sid}{app_secret}'.format(
        expire=form.cleaned_data['expire'],
        mid=form.cleaned_data['mid'],
        secret=form.cleaned_data['secret'],
        sid=form.cleaned_data['sid'],
        app_secret=VkConfig.CLIENT_SECRET
    )
    sig = MD5.new(bytes(sig_raw, 'utf-8')).hexdigest()

    if sig != form.cleaned_data['sig']:
        form.add_error(None, 'invalid form')
        return form.render_errors_to_json()

    try:
        user = User.objects.get(vk_id=form.cleaned_data['mid'])
    except User.DoesNotExist:
        try:
            user = User.objects.create(name=form.cleaned_data['name'], vk_id=form.cleaned_data['mid'])
        except DjDBError:
            form.add_error(None, 'invalid form')
            return form.render_errors_to_json()

    auth.signin(request, user)
    return redirect(Factory.URLBuilder.build_user_page_url(user.id))


@decor_authenticated
@decor_valid_form(SignoutForm)
def signout(request, form):
    user_id = request.user.id
    auth.signout(request)
    return redirect(Factory.URLBuilder.build_user_page_url(user_id))


@decor_valid_form(DownloadCalendarForm)
def download_calendar(request, form):
    try:
        dates = CalendarData.objects.filter(
            user=form.cleaned_data['owner_id'],
            date__gte=form.cleaned_data['date_from'],
            date__lte=form.cleaned_data['date_to']
        )

        return json_response({str(d.date): [d.value, int(d.timewarn)] for d in dates})
    except Exception as e:
        if djconfig.DEBUG:
            raise e
        form.add_error(None, 'invalid form')
        return form.render_errors_to_json()


@decor_authenticated
@decor_valid_form(UploadCalendarForm)
def upload_calendar(request, form):
    try:
        try:
            CalendarData.objects.create(
                user=request.user,
                date=form.cleaned_data['date'],
                value=form.cleaned_data['value'],
                timewarn=form.cleaned_data['timewarn']
            )
        except IntegrityError:  # duplicate of key
            CalendarData.objects\
                .filter(user=request.user, date=form.cleaned_data['date'])\
                .update(value=form.cleaned_data['value'], timewarn=form.cleaned_data['timewarn'])
    except Exception as e:
        if djconfig.DEBUG:
            raise e
        form.add_error(None, 'form invalid')
        return form.render_errors_to_json()

    return json_response()


@decor_authenticated
@decor_valid_form(UpdateSettingsForm)
def update_settings(request, form):
    request.user.name = form.cleaned_data['name']
    request.user.calendar_type = form.cleaned_data['calendar_type']
    request.user.save()
    return json_response()
