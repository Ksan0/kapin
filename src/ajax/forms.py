from datetime import datetime

from django import forms

from src.conf.config import CalendarConfig, AjaxResponseConfig
from src.main import form
from src.factory import Factory

from .form_fields import DateField, BooleanField
from .utils import json_response


class AjaxFormsHolder(object):
    forms = []


def ajax_form(cls):
    AjaxFormsHolder.forms.append(cls())
    return cls


class AjaxForm(form.Form):
    def render_errors_to_json(self):
        return json_response({e: self.errors[e][0] for e in self.errors}, AjaxResponseConfig.AJAX_RESPONSE_CODE_ERROR)


class AjaxEmptyForm(AjaxForm):
    action = BooleanField()


@ajax_form
class SigninVkForm(AjaxForm):
    # signin fields
    expire = forms.CharField()
    mid = forms.IntegerField()
    secret = forms.CharField()
    sid = forms.CharField()
    sig = forms.CharField()

    # signup fields
    name = forms.CharField(max_length=50)

    def __init__(self, **kwargs):
        super().__init__(Factory.URLBuilder.build_ajax_signin_vk_name(), **kwargs)


@ajax_form
class SignoutForm(AjaxEmptyForm):
    def __init__(self, **kwargs):
        super().__init__(Factory.URLBuilder.build_ajax_signout_name(), **kwargs)


@ajax_form
class DownloadCalendarForm(AjaxForm):
    owner_id = forms.IntegerField()
    date_from = DateField()
    date_to = DateField()

    def __init__(self, **kwargs):
        super().__init__(Factory.URLBuilder.build_ajax_download_calendar_name(), **kwargs)

    def clean_date_to(self):
        delta = (self.cleaned_data['date_to'] - self.cleaned_data['date_from']).days

        if delta < 0:
            raise forms.ValidationError('date_from > date_to')
        if delta >= CalendarConfig.MAX_DOWNLOAD_DAYS_DELTA:
            raise forms.ValidationError('Too many days for download')

        return self.cleaned_data['date_to']


@ajax_form
class UploadCalendarForm(AjaxForm):
    date = DateField()
    value = forms.IntegerField(min_value=0, max_value=4)
    timewarn = BooleanField()

    def __init__(self, **kwargs):
        super().__init__(Factory.URLBuilder.build_ajax_upload_calendar_name(), **kwargs)

    def clean_date(self):
        delta = (datetime.now().date() - self.cleaned_data['date']).days

        if delta < -1:
            # послезавтрашний день пытаются поставить
            raise forms.ValidationError('Try it tomorrow')
        if delta > CalendarConfig.MAX_UPLOAD_DAYS_DELTA:
            # слишком далекое прошлое
            raise forms.ValidationError('Too old date')

        return self.cleaned_data['date']

    def clean_timewarn(self):
        return self.cleaned_data['timewarn'] or (datetime.now().date() - self.cleaned_data['date']).days > CalendarConfig.WARN_UPLOAD_DAYS_DELTA_SERVER


@ajax_form
class UpdateSettingsForm(AjaxForm):
    name = forms.CharField(max_length=50)
    calendar_type = forms.IntegerField(min_value=CalendarConfig.CALENDAR_TYPE_MIN_VALUE, max_value=CalendarConfig.CALENDAR_TYPE_MAX_VALUE)

    def __init__(self, **kwargs):
        super().__init__(Factory.URLBuilder.build_ajax_update_settings_name(), **kwargs)
