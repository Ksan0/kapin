from django import forms
from django.utils import six


# noinspection PyAbstractClass
class CheckboxInput(forms.CheckboxInput):
    def value_from_datadict(self, data, files, name):
        if name not in data:
            # A missing value means False because HTML form submission does not
            # send results for unselected checkboxes.
            return False
        value = data.get(name)
        # Translate true and false strings to boolean values.
        values = {'true': True, 'false': False, '1': True, '0': False}
        if isinstance(value, six.string_types):
            value = values.get(value.lower(), value)
        return bool(value)
