from django import forms
from .form_widgets import CheckboxInput


# noinspection PyAbstractClass
class DateField(forms.DateField):
    def __init__(self, input_formats=None, *args, **kwargs):
        if input_formats is None:
            input_formats = ('%Y-%m-%d',)
        super().__init__(input_formats, *args, **kwargs)


class BooleanField(forms.BooleanField):
    def __init__(self, **kwargs):
        if 'widget' not in kwargs:
            kwargs['widget'] = CheckboxInput
        if 'required' not in kwargs:
            kwargs['required'] = False
        super().__init__(**kwargs)
