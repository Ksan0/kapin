from django.core.management import BaseCommand

from src.manage.utils import system, rsync, ssh


# noinspection PyMethodMayBeStatic
class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('mode', type=str)

    def handle(self, *args, **options):
        mode = options['mode']
        if mode == 'dev':
            from src.manage.config import DeployDevConfig as Config
        elif mode == 'prod':
            from src.manage.config import DeployProdConfig as Config
        else:
            raise Exception('unknown mode')

        print('deploying {} BEGIN'.format(mode))

        system('rm -rf build; mkdir build')

        # make 'src'
        rsync('a', 'src build/', exc=['*.pyc', '__pycache__', 'config.py', 'djconfig.py', 'conf/*/*'])

        # make 'template'
        rsync('a', 'template build/')

        # make 'static'
        system('cd src-static && grunt')
        rsync('a', 'src-static/ build/static/', exc=['node_modules/', '/js/', '/css/', 'Gruntfile.js', 'package.json'])
        rsync('a', 'src-static/js/dist/ build/static/js/')
        rsync('a', 'src-static/css/dist/ build/static/css/')

        # wsgi.py, manage.py
        rsync('a', 'wsgi.py build/')
        rsync('a', 'manage.py build/')

        # apply config
        if mode == 'dev':
            rsync('a', 'src/conf/*config.py build/src/conf/')
        elif mode == 'prod':
            rsync('a', 'src/conf/prod/*config.py build/src/conf/')

        # rsync to server
        ssh(Config.USER, Config.HOST, 'rm -rf kapin')

        inc, exc = (['*.min.js', '*.min.css', 'iconfont/*'], ['*.js', '*.css']) if mode == 'prod' else ([], [])
        exc.append('*~')
        rsync('rvz', 'build/ {user}@{host}:kapin'.format(user=Config.USER, host=Config.HOST), inc=inc, exc=exc)

        # restart uwsgi
        ssh(Config.USER, Config.HOST, 'bash kapin/src/manage/management/commands/restart.sh')

        print('deploying {} END'.format(mode))
