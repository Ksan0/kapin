uwsgi --stop pid/uwsgi.pid

if [ $? -eq 0 ]; then
    echo 'Stopping uwsgi'
    while [ -a pid/uwsgi.pid ]; do
        echo 'Waiting for uwsgi stop'
        sleep 1
    done
fi

echo 'Starting uwsgi'
uwsgi --ini conf/uwsgi.ini

until [ -a true ]; do
    echo 'Waiting for file "true"'
    sleep 1
done
rm true