from .url_builder import URLBuilder
from .session_storage import SessionStorage
from .db_cache import DBCache


class FactoryBuilder(object):
    def __init__(self):
        self.URLBuilder = URLBuilder()

        self.SessionStorage = SessionStorage()
        self.DBCache = DBCache()
