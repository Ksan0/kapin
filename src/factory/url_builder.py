# noinspection PyMethodMayBeStatic
class URLBuilder(object):

    # --- page urls ---
    def build_index_page_pattern(self):
        return r'^$'

    def build_index_page_url(self):
        return '/'

    def build_hello_page_pattern(self):
        return r'^hello$'

    def build_hello_page_url(self):
        return '/hello'

    def build_user_page_pattern(self):
        return r'^id([0-9]+)$'

    def build_user_page_url(self, user_id):
        return '/id{}'.format(user_id)

    def build_settings_page_pattern(self):
        return r'^settings$'

    def build_settings_page_url(self):
        return '/settings'

    # --- folder urls ---
    def build_ajax_folder_pattern(self):
        return r'^ajax/'

    # --- ajax names ---
    def build_ajax_signin_vk_name(self):
        return 'ajax_signin_vk'

    def build_ajax_signout_name(self):
        return 'ajax_signout'

    def build_ajax_download_calendar_name(self):
        return 'ajax_download_calendar'

    def build_ajax_upload_calendar_name(self):
        return 'ajax_upload_calendar'

    def build_ajax_update_settings_name(self):
        return 'ajax_update_settings'
