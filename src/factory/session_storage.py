import redis
import binascii

from Crypto import Random

from src.conf.config import MainConfig, RedisConfig


class SessionStorage(object):
    def __init__(self):
        self.__redis = redis.Redis(unix_socket_path=RedisConfig.UNIX_SOCKET_PATH, db=RedisConfig.SESSION_STORAGE_DBNUM)

    def generate_session(self, uid):
        sid = self.__redis.get(uid)
        # умножение на 2 из-за того, что hexlify удлиняет строку в 2 раза
        if sid is None or len(sid) != MainConfig.AUTH_SESSION_KEY_BYTES * 2:
            sid = binascii.hexlify(Random.get_random_bytes(MainConfig.AUTH_SESSION_KEY_BYTES))
            self.__redis.set(uid, sid)
        return sid.decode('utf-8')

    def validate_session(self, uid, sid):
        store_sid = self.__redis.get(uid)
        return store_sid is not None and sid == store_sid.decode('utf-8')
