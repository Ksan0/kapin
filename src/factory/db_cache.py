import redis
from django.core import serializers

from src.conf.config import RedisConfig


# noinspection PyMethodMayBeStatic
class DBCache(object):
    def __init__(self):
        self.__redis = redis.Redis(unix_socket_path=RedisConfig.UNIX_SOCKET_PATH, db=RedisConfig.DB_CACHE_DBNUM)

    def get_object(self, obj_cls, obj_id, expire):
        key = self.__build_key(obj_cls, obj_id)
        obj_json = self.__redis.get(key)
        if obj_json is None:
            obj = obj_cls.objects.get(id=obj_id)
            self.__redis.set(key, serializers.serialize('json', [obj]), expire)
        else:
            obj = next(serializers.deserialize('json', obj_json)).object
            self.__redis.expire(key, expire)
        return obj

    def update_object(self, obj, expire):
        key = self.__build_key(obj.__class__, obj.id)
        if self.__redis.exists(key):
            self.__redis.set(key, serializers.serialize('json', [obj]), expire)

    def __build_key(self, obj_cls, obj_id):
        # обратный порядок для увеличения производительности (хотя я не уверен, что она увеличится)
        # объектов с id=1234 заведомо меньше, чем объектов с __module__=src.db
        return '{}.{}.{}'.format(obj_id, obj_cls.__name__, obj_cls.__module__)
