module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                separator: '\n\n'
            },
            dist: {
                files: {
                    'js/dist/kapin.js': ['js/src/script.js', 'js/src/**/*.js'],
                    'css/dist/kapin.css': ['css/src/*.css']
                }
            }
        },
        uglify: {
            dist: {
                files: {
                    'js/dist/kapin.min.js': ['js/dist/kapin.js']
                }
            }
        },
        jshint: {
            files: ['Gruntfile.js', 'js/src/**/*.js'],
            options: {
                // options here to override JSHint defaults
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                }
            }
        },
        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'css/dist',
                    src: ['*.css', '!*.min.css'],
                    dest: 'css/dist',
                    ext: '.min.css'
                }]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('default', ['jshint', 'concat', 'uglify', 'cssmin']);
};
