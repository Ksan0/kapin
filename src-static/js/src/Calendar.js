KP.Calendar = function ($tableTitle, $tableHolder, viewerId, ownerId, date, type) {
    this.$tableTitle = $tableTitle;
    this.$tableHolder = $tableHolder;

    this.viewerId = viewerId;
    this.ownerId = ownerId;
    this.date = date;
    this.type = type;

    KP.utils.clearStorage();
};

KP.Calendar.prototype.update = function (ownerId, date) {
    if (ownerId) {
        this.ownerId = ownerId;
    }
    if (date) {
        this.date = date;
    }
};

KP.Calendar.prototype.show = function (state) {
    var self = this,
        showDate = KP.utils.getShowDateBegin(self.date),
        downloadDateBegin = KP.utils.getDownloadDateBegin(showDate, 7*6),
        downloadDateEnd = KP.utils.getDownloadDateEnd(showDate, 7*6);

    function __createAndReplaceTable() {
        self.$tableHolder
            .empty()
            .append(KP.utils.createCalendarTable(self.date, showDate, 7, 6, self.viewerId === self.ownerId, self.type));
    }

    if (state === 'push') {
        KP.utils.pushPageState(self.ownerId, self.date, false);
    } else if (state === 'replace') {
        KP.utils.pushPageState(self.ownerId, self.date, true);
    }

    self.$tableTitle.html(KP.utils.getCalendarTitle(self.date));

    if (downloadDateBegin !== null && downloadDateEnd !== null) {
        var loadElem = document.createElement('div');
        loadElem.className = 'table__loading';

        self.$tableHolder
            .empty()
            .html(loadElem);
        KP.utils.downloadCalendar(self.ownerId, downloadDateBegin, downloadDateEnd, self.type, __createAndReplaceTable);
    } else {
        __createAndReplaceTable();
    }
};

KP.Calendar.prototype.showPrev = function () {
    this.date.setMonth(this.date.getMonth() - 1);
    this.show('push');
};

KP.Calendar.prototype.showNext = function () {
    this.date.setMonth(this.date.getMonth() + 1);
    this.show('push');
};