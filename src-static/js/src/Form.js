KP.Form = function ($form, url) {
    this.$form = $form;
    this.url = url;
};

KP.Form.prototype.setFieldValue = function (field, value) {
    this.$form.find('[name=' + field + ']').val(value);
    return this;
};

KP.Form.prototype.getFieldOption = function (field, option) {
    return this.$form.find('[name=' + field + ']').attr(option);
};

KP.Form.prototype.send = function (success) {
    // jshint ignore:start
    $.ajax({
        method: 'POST',
        url: this.url,
        data: this.$form.serialize(),
        success: function (data) {
            if (data['code'] == KP.conf.get('AjaxResponseConfig', 'AJAX_RESPONSE_CODE_SUCCESS')) {
                if (success) {
                    success(data['data']);
                }
            } else if (data['code'] == KP.conf.get('AjaxResponseConfig', 'AJAX_RESPONSE_CODE_REDIRECT')) {
                window.location.href = data['data'];
            } else if (data['code'] == KP.conf.get('AjaxResponseConfig', 'AJAX_RESPONSE_CODE_ERROR')) {
                console.log('ajax form error: ' + data['data'].toString());
            }
        }
    });
    // jshint ignore:end
};